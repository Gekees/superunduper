#! /usr/bin/env python3
 
import sys, os, hashlib, re

def main():
    if(("-d" in sys.argv) && ("-f" in sys.argv)):
        directory = sys.argv[sys.argv.index("-d")+1]
        output = sys.argv[sys.argv.index("-f")+1]
    elif(

    try:
        outputIndex = sys.argv.index("-f")+1
        outputLocation = sys.argv[outputIndex]
        output = true
    except ValueError as err:
        output = false
    directory = sys.argv[1]
    files = getFiles(directory)
#    print(files)
    checkFiles(files)

def doHash(targFile):
    md5 = hashlib.md5()
    try:
        with open(targFile, "rb") as f:
            for i in iter(lambda: f.read(1024), b""):
                md5.update(i)
    except FileNotFoundError as err:
        pass
    return md5.hexdigest()

def getFiles(directory):
    fileList = []
    try:
        for mainDir, subDir, allFiles in os.walk(directory):
            for files in allFiles:
                fileList.append(os.path.join(mainDir, files))
        print("\nTotal of the files to be handled:\n\t\t"+str(len(fileList))+"\n\n")
        return fileList
    except FileNotFoundError as err:
        print ("Invalid target directory.")
        exit()

def checkFiles(fileList):
    while(len(fileList) > 0):
        subjectFiles = []
        tempHash = ""
        currentHash = ""
        currentHash = doHash(fileList[0])
        for each in fileList:
            try:
                tempHash = doHash(each)
                if(tempHash == currentHash):
#                    print("Hash is equal"+each)
                    subjectFiles.append(each)
            except FileNotFoundError as err:
                print("That file does not exist.")
                pass
        if "-p" in sys.argv:
            removeFiles(subjectFiles)
        else:
            formattedPrint(subjectFiles)
        for i in subjectFiles:
#            print(i)
            fileList.remove(i)
    exit()

def removeFiles(subjectFiles):
    count = 1
    for entry in subjectFiles:
        print("\n\t"+str(count)+"."+entry)
        count += 1
    sys.stdout.write("\n\n Type the number of the file(s) that you want to delete, separated by spaces.")
    print(" e.g. 1 5 7 2\n just press Enter to continue without deleting. Type \"all for all.")
    chosenFiles = input(":").split()
    if(chosenFiles == "all".lower()):
        for every in subjectFiles:
            os.remove(every)
    else:
        for number in chosenFiles:
            try:
                realNumber = int(number)
                if((realNumber > 0) and (realNumber < len(subjectFiles))):
                    os.remove(subjectFiles[realNumber-1])
            except:
                pass

def formattedPrint(subjectFiles):
    for i in subjectFiles:
        print("\n"+i)
    print("\n\n")
def writeFile()
main()    
    
    
